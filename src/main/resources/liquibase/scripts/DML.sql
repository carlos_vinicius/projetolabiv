--liquibase formatted sql


CREATE TABLE `autorizacao` ( `idAutorizacao` INT NOT NULL AUTO_INCREMENT,  `nomeAutorizacao` VARCHAR(20) NOT NULL,  PRIMARY KEY (`idAutorizacao`),  UNIQUE KEY `UNI_AUT_NOME` (`nomeAutorizacao`));

CREATE TABLE `usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nomeUsuario` VARCHAR(20) NOT NULL,
  `senha` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY UNI_USUARIO_NOME (`nomeUsuario`)
);
CREATE TABLE IF NOT EXISTS usuarioAutorizacao (
  idUsuario INT NOT NULL AUTO_INCREMENT,
  idAutorizacao INT NOT NULL AUTO_INCREMENT
  PRIMARY KEY (idUsuario, idAutorizacao),
  FOREIGN KEY AUT_USUARIO_FK (idUsuario) REFERENCES usuario (idUsuario) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY AUT_AUTORIZACAO_FK (idAutorizacao) REFERENCES autorizacao (idAutorizacao) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS movimento (
  idMovimento INT NOT NULL AUTO_INCREMENT,
  dataMovimento DATE NOT NULL,
  saldo DOUBLE(7,2) NOT NULL,
  PRIMARY KEY (idMovimento));

CREATE TABLE IF NOT EXISTS tipoProduto (
  idTipoProduto INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50) NOT NULL,
  tipo VARCHAR(150) NOT NULL,
  descricao VARCHAR(250),
  PRIMARY KEY (idTipoProduto));
  
CREATE TABLE IF NOT EXISTS produto (
  idProduto INT NOT NULL AUTO_INCREMENT,
  idTipoProduto INT NOT NULL,
  preco DOUBLE(5,2),
  nomeProduto VARCHAR(45) NOT NULL,
  fabricante VARCHAR(45) NOT NULL,
  PRIMARY KEY (idProduto),
  FOREIGN KEY (idTipoProduto) REFERENCES tipoProduto (idTipoProduto)
  );
  
  
CREATE TABLE IF NOT EXISTS cliente (
  idCliente INT NOT NULL AUTO_INCREMENT,
  nomeCliente VARCHAR(45),
  sobreNomeCliente VARCHAR(45),
  emailCliente VARCHAR(45),
  cpf VARCHAR(45) NOT NULL,
  enderecoCliente VARCHAR(45) NOT NULL,
  dataCadastro DATE NOT NULL,
  dataNascimento DATE NOT NULL,
  PRIMARY KEY (idCliente));

CREATE TABLE IF NOT EXISTS funcionario (
  idFuncionario INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50) NOT NULL,
  sobreNome VARCHAR(50) NOT NULL,
  email VARCHAR(150) NOT NULL,
  cpf VARCHAR(150) NOT NULL,
  endereco VARCHAR(45),
  salario DOUBLE(6,2),
  dataAdmissao DATE NOT NULL,
  dataNascimento DATE NOT NULL,
  PRIMARY KEY (idFuncionario),
  UNIQUE KEY UNI_funcionario_email (email)
  );

  
CREATE TABLE IF NOT EXISTS registro (
idRegistro INT NOT NULL AUTO_INCREMENT,
idCliente INT NOT NULL,
idFuncionario INT NOT NULL,
idProduto INT NOT NULL,
formaDePagamento VARCHAR(45) NOT NULL,
dataRegistro DATE,
quantidade INT NOT NULL,
PRIMARY KEY (idRegistro),
INDEX fk_registro_cliente1_idx (idCliente ASC),
INDEX fk_registro_funcionario1_idx (idFuncionario ASC),
INDEX fk_registro_produto1_idx (idProduto ASC),
FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
FOREIGN KEY (idFuncionario) REFERENCES funcionario (idFuncionario),
FOREIGN KEY (idProduto) REFERENCES produto (idProduto));

CREATE TABLE IF NOT EXISTS fornecedor (
idFornecedor INT NOT NULL AUTO_INCREMENT,
nomeFornecedor VARCHAR(45) NOT NULL,
emailFornecedor VARCHAR(150),
enderecoFornecedor VARCHAR(100),
PRIMARY KEY (idFornecedor));
  
CREATE TABLE IF NOT EXISTS estoque(
idEstoque INT NOT NULL AUTO_INCREMENT,
quantidade INT NOT NULL,
idProduto INT NOT NULL,
idFornecedor INT NOT NULL,
PRIMARY KEY (idEstoque),
INDEX fk_estoque_fornecedor1_idx (idFornecedor ASC),
INDEX fk_estoque_produto1_idx (idProduto ASC),
UNIQUE KEY estoque_produto_fornecedor (idProduto, idFornecedor),
FOREIGN KEY (idFornecedor)
REFERENCES fornecedor (idFornecedor),
FOREIGN KEY (idProduto)
REFERENCES produto (idProduto));

ALTER TABLE usuario ADD idFuncionario INT;
ALTER TABLE usuario ADD CONSTRAINT id_fk_funcionario FOREIGN KEY(idFuncionario) REFERENCES funcionario(idFuncionario);
ALTER TABLE usuario ADD CONSTRAINT UNI_FUNCIONARIO UNIQUE (idFuncionario);