--liquibase formatted sql


INSERT INTO usuario(nomeUsuario, senha) VALUES('teste', MD5('teste'));
INSERT INTO usuario(nomeUsuario, senha) VALUES('admin', MD5('admin'));
INSERT INTO autorizacao(nomeAutorizacao) VALUES('ROLE_USUARIO');
INSERT INTO autorizacao(nomeAutorizacao) VALUES('ROLE_ADMIN');

INSERT INTO usuarioAutorizacao(idUsuario, idAutorizacao)
SELECT idUsuario, idAutorizacao
FROM usuario, autorizacao
WHERE nomeUsuario = 'teste'
AND nomeAutorizacao = 'ROLE_USUARIO';

INSERT INTO usuarioAutorizacao(idUsuario, idAutorizacao)
SELECT idUsuario, idAutorizacao
FROM usuario, autorizacao
WHERE nomeUsuario = 'admin';

insert into movimento(idMovimento,dataMovimento,saldo)values(1,curdate(),5000.00); 
insert into movimento(idMovimento,dataMovimento,saldo)values(2,curdate(),6000.00);
insert into movimento(idMovimento,dataMovimento,saldo)values(3,curdate(),5500.00);
insert into movimento (idMovimento, dataMovimento,saldo)values(4,CURDATE() ,900.00);
insert into movimento (idMovimento, dataMovimento,saldo)values(5,CURDATE(),1200.00);
insert into movimento (idMovimento, dataMovimento,saldo)values(6,CURDATE(),-700.00);
insert into movimento (idMovimento, dataMovimento,saldo)values(7,CURDATE(),1900.00);
insert into movimento (idMovimento, dataMovimento,saldo)values(8,CURDATE(),-9000.00);


insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(1,'chapeu','panama','chapeu_bege');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(2,'vaso','barro','vaso_do_ghost');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(3,'Jaqueta','Vestuario','Jaqueta de poliester');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(4,'Rel�gio','Vestuario','Rel�gio de prata');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(5,'T�nis','Cal�ado','T�nis de corrida');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(6,'T�nis','Cal�ado','T�nis de corrida');
insert into tipoProduto(idTipoProduto,nome,tipo,descricao)values(7,'Microsystem','Eletr�nico','Microsystem mp3 pendrive sd card');


insert into produto(idProduto,idTipoProduto,preco,nomeProduto,fabricante)values(1,1,110.00,'chapelex','marfino');
insert into produto(idProduto,idTipoProduto,preco,nomeProduto,fabricante)values(2,7,200.00,'vasex','argilo');
insert into produto(idProduto,idTipoProduto,preco,nomeProduto,fabricante)values(3,2,250.00,'rolex','pinteba');
insert into produto (idproduto,idTipoProduto,preco,nomeProduto,fabricante)values(4,2,500.00,'Jaqueta Supreme','Supreme');
insert into produto (idproduto,idTipoProduto,preco,nomeProduto,fabricante)values(5,3,800.00,'Rel�gio de pulso','Orient');
insert into produto (idproduto,idTipoProduto,preco,nomeProduto,fabricante)values(6,4,600.00,'Nike air max 90','Nike');
insert into produto (idproduto,idTipoProduto,preco,nomeProduto,fabricante)values(7,5,500.00,'NB 998','New Balance');
insert into produto (idproduto,idTipoProduto,preco,nomeProduto,fabricante)values(8,6,500.00,'Microsystem 3500 wts','Philco');

insert into cliente(idCliente,nomeCliente,sobreNomeCliente,dataNascimento,emailCliente,cpf,enderecoCliente,dataCadastro)
values(1,'creitu','rego','1980-3-20','creitugatinho@gmail.com','72375358744', 'rua 1',curdate());
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,dataNascimento,emailCliente,cpf,enderecoCliente,dataCadastro)
values(2,'gilberto','gil','1942-7-26','quedeusdeu@gmail.com', '97257375884', 'rua 2',curdate());
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,dataNascimento,emailCliente,cpf,enderecoCliente,dataCadastro)
values(3,'dunga','burro','1975-5-22','dungaburro@gmail.com','96969696969', 'rua 3',curdate());
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,emailCliente,enderecoCliente,dataCadastro,cpf,dataNascimento)
values(4,'creitu','rego','creitugatinho@gmail.com','rua 1',curdate(), '123345','1975-5-22');
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,emailCliente,enderecoCliente,dataCadastro,cpf,dataNascimento)
values(5,'gilberto','gil','quedeusdeu@gmail.com', 'rua 2',curdate(), '16345','1975-5-22');
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,emailCliente,enderecoCliente,dataCadastro,cpf,dataNascimento)
values(6,'dunga','burro','dungaburro@gmail.com', 'rua 3',curdate(), '121645','1975-5-22');
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,emailCliente,enderecoCliente,dataCadastro,cpf,dataNascimento)
values(7,'djonga','mc','djongamc@gmail.com', 'rua  minas',curdate(), '121145','1975-5-22');
insert into cliente(idCliente,nomeCliente,sobreNomeCliente,emailCliente,enderecoCliente,dataCadastro,cpf,dataNascimento)
values(8,'peidru','junior','peidruhu@gmail.com', 'rua dos carpano',curdate(), '123455','1975-5-22');  


insert into funcionario(idFuncionario,nome,sobreNome,email,dataNascimento,cpf,endereco,salario,dataAdmissao)
values(1,'baco','exudoblues','baco999@gmail.com','1996-5-20','444656987715','rua bahia',1000.00,curdate());
insert into funcionario(idFuncionario,nome,sobreNome,email,dataNascimento,cpf,endereco,salario,dataAdmissao)
values(2,'diomedes','chinaski','sulicidio@gmail.com','1990-6-24','56565656565','rua do aprendiz',1500.00,curdate());
insert into funcionario(idFuncionario,nome,sobreNome,email,dataNascimento,cpf,endereco,salario,dataAdmissao)
values(3,'nada','lete','nadalete@gmail.com','1985-5-21','87878787878','rua da fatec',5000.00,curdate());
insert into funcionario(idFuncionario,nome,sobreNome,email,cpf,endereco,salario,dataAdmissao,dataNascimento)
values(4,'baco','exudoblues','baco989@gmail.com','444656987715','rua bahia',1000.00,curdate(),'1996-5-20');
insert into funcionario(idFuncionario,nome,sobreNome,email,cpf,endereco,salario,dataAdmissao,dataNascimento)
values(5,'diomedes','chinaski','sulicidio1@gmail.com','56565656565','rua do aprendiz',1500.00,curdate(),'1990-6-24');
insert into funcionario(idFuncionario,nome,sobreNome,email,cpf,endereco,salario,dataAdmissao,dataNascimento)
values(6,'nada','lete','vish@gmail.com','87878787878','rua normal',5000.00,curdate(),'1985-5-21');
insert into funcionario(idFuncionario,nome,sobreNome,email,cpf,endereco,salario,dataAdmissao,dataNascimento)
values(7,'lucio','fernando','lucifer1@gmail.com','99999999999','rua do bafom�',9000.00,curdate(),'1966-6-06');
insert into funcionario(idFuncionario,nome,sobreNome,email,cpf,endereco,salario,dataAdmissao,dataNascimento)
values(8,'tulio','fernando','topper2@gmail.com','99990097999','rua do bafo',9000.00,curdate(),'1970-6-06');





insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro, quantidade)
values(1,1,1,1,'a vista',curdate(), 30);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro, quantidade)
values(2,2,2,2,'a vista',curdate(), 40);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro, quantidade)
values(3,3,3,3,'a prazo',curdate(), 50);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro,quantidade)
values(4,4,4,4,'a vista',curdate(),5);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro,quantidade)
values(5,5,5,5,'a vista',curdate(),10);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro,quantidade)
values(6,6,6,6,'a prazo',curdate(),15);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro,quantidade)
values(7,7,7,7,'a prazo',curdate(),20);
insert into registro(idRegistro,idCliente,idFuncionario,idProduto,formaDePagamento,dataRegistro,quantidade)
values(8,8,8,8,'a prazo',curdate(),25);


insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(1,'juaquim','juaquim@gmail.com','rua dos astecas');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(2,'janaina','jana@gmail.com','rua das mulheres');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(3,'tobias','tobias@gmail.com','rua da zika');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(4,'juaquim','juaquim2@gmail.com','rua dos astecas');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(5,'janaina','jana1@gmail.com','rua das mulheres');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(6,'tobias','tobias1@gmail.com','rua da zika');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(7,'tobias','tobias2@gmail.com','rua da zika');
insert into fornecedor(idFornecedor,nomeFornecedor,emailFornecedor,enderecoFornecedor)
values(8,'tobias','tobias3@gmail.com','rua da zika');


insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(1,100,1,1);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(2,200,2,2);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(3,300,3,3);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(4,100,4,4);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(5,200,5,5);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(6,300,6,6);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(7,400,7,7);
insert into estoque(idEstoque,quantidade,idProduto,idFornecedor)values(8,500,8,8);


                                                      