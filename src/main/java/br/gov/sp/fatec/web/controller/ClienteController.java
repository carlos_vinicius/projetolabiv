package br.gov.sp.fatec.web.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.Cliente;
import br.gov.sp.fatec.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@ApiOperation(value = "Adiciona um Cliente", response = Cliente.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Adicionado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/cliente/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Cliente> add(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) {
		cliente.setDataCadastro(new Date());
		clienteService.save(cliente);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/cliente/{id}").buildAndExpand(cliente.getId()).toUri());
		return new ResponseEntity<Cliente>(header, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um Cliente", response = Cliente.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/cliente/update/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Cliente> updateById(@PathVariable Integer id, @RequestBody Cliente cliente,
			UriComponentsBuilder ucBuilder) {
		Cliente newCliente = clienteService.updateById(id, cliente);
		HttpHeaders header = new HttpHeaders();

		if (newCliente != null){
			header.setLocation(ucBuilder.path("/cliente/{id}").buildAndExpand(cliente.getId()).toUri());
			return new ResponseEntity<Cliente>(header, HttpStatus.OK);
		}
		else{
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Alcanca um Cliente", response = Cliente.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/cliente/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Cliente> findById(@PathVariable Integer id) {
		Cliente cliente = clienteService.findOne(id);
		
		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Cliente>(cliente, HttpStatus.FOUND);
	}
	
	@ApiOperation(value = "Remove um Cliente", response = Cliente.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Removido com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/cliente/delete/{id}", method = RequestMethod.PUT)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {
		
		Cliente cliente = clienteService.findOne(id);
		if (cliente == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		clienteService.delete(cliente);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Alcanca todos os Clientes", response = Cliente.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/cliente/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Cliente>> listAll() {
		List<Cliente> clienteList = clienteService.listAll();

		if (clienteList.isEmpty()) {
			return new ResponseEntity<List<Cliente>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Cliente>>(clienteList, HttpStatus.OK);
	}	
}
