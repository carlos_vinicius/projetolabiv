package br.gov.sp.fatec.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.Estoque;
import br.gov.sp.fatec.service.EstoqueService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;

@RestController
@PreAuthorize("isAuthenticated()")
public class EstoqueController {

	private static final String EXCEPTION_JAVA = "exceptionJava";
	
	@Autowired
	private EstoqueService estoqueService;
	
	@ApiOperation(value = "Adiciona uma entrada de Estoque", response = Estoque.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Adicionado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/estoque/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estoque> add(@RequestBody Estoque estoque, UriComponentsBuilder ucBuilder) {

		estoqueService.save(estoque);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/estoque/{id}").buildAndExpand(estoque.getId()).toUri());
		return new ResponseEntity<Estoque>(header, HttpStatus.OK);

	}
	
	@ApiOperation(value = "Alcanca uma entrada de Estoque", response = Estoque.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/estoque/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Estoque> getById(@PathVariable Integer id) {
		Estoque estoque = estoqueService.findOne(id);

		if (estoque == null) {
			return new ResponseEntity<Estoque>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Estoque>(estoque, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Repoe os estoques confome a quantidade inserida", response = Estoque.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Reposto com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/estoque/reposicao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> repor(
			@RequestParam(value = "produtoid", required = false)  Integer produtoId,
			@RequestParam(value = "quantidade", required = false)  Integer quantidade) {

		if (produtoId == null) {
			estoqueService.reporEstoques(quantidade);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		if(quantidade == null){
			HttpHeaders headers = new HttpHeaders();
			headers.add(EXCEPTION_JAVA, "Insira uma quantidade a ser reposta");
			return new ResponseEntity<Void>(headers, HttpStatus.BAD_REQUEST);
		}

		try {
			estoqueService.reporEstoqueProduto(produtoId, quantidade);
		} catch (NotFoundException e) {
			e.printStackTrace();
			HttpHeaders headers = new HttpHeaders();
			headers.add(EXCEPTION_JAVA, e.getMessage());
			return new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
