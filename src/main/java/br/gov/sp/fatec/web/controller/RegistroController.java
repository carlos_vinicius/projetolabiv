package br.gov.sp.fatec.web.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.fatec.model.Registro;
import br.gov.sp.fatec.service.RegistroService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;

@RestController
@PreAuthorize("isAuthenticated()")
public class RegistroController {

	private static final String EXCEPTION_JAVA = "exceptionJava";
	
	@Autowired
	private RegistroService registroService;

	
	@ApiOperation(value = "Efetua uma venda de um produto", response = Registro.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Venda efetuada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/registro/efetuarvenda", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Registro> criarRegistro(@RequestBody Registro registro) {
		try {
			registroService.efetuaVenda(registro);
			return new ResponseEntity<Registro>(registro , HttpStatus.OK);
		} catch (NoSuchElementException|IllegalArgumentException|NotFoundException e) {
			HttpHeaders headers = new HttpHeaders();
			headers.add(EXCEPTION_JAVA, e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<Registro>(headers, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@ApiOperation(value = "Alcanca um registro de venda", response = Registro.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/registro/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Registro> findById(@PathVariable Integer id) {
		Registro registro = registroService.findOne(id);

		if (registro == null) {
			return new ResponseEntity<Registro>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Registro>(registro, HttpStatus.OK);
	}

	@ApiOperation(value = "Remove um registro de venda", response = Registro.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Removida com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/registro/delete/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {
		Registro registro = registroService.findOne(id);
		if (registro == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		registroService.delete(registro);
		return new ResponseEntity<Void>(HttpStatus.OK);	

	}
	
	@ApiOperation(value = "Lista todos os registros de venda", response = Registro.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Listado todos com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/registro/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Registro>> listAll() {
		List<Registro> registroList = registroService.listAll();

		if (registroList.isEmpty()) {
			return new ResponseEntity<List<Registro>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Registro>>(registroList, HttpStatus.OK);
	}
}