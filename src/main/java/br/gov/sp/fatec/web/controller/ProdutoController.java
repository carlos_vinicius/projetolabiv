package br.gov.sp.fatec.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;
import br.gov.sp.fatec.service.ProdutoService;
import br.gov.sp.fatec.service.TipoProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@PreAuthorize("isAuthenticated()")
@Api(value="ProdutoController")
public class ProdutoController {

	private static final String EXCEPTION_HEADER = "exceptionJava";

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private TipoProdutoService tipoProdutoService;


	@ApiOperation(value = "Get just one Product data", response = Produto.class)
	@ApiResponses(value = {
	@ApiResponse(code = 200, message = "Successfully retrieved list"),
	@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	
	@RequestMapping(value = "/produto/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> add(@RequestBody Produto produto, UriComponentsBuilder ucBuilder) {

		TipoProduto tipoProduto = produto.getTipoProduto();
		TipoProduto tipoProdutoFound = tipoProdutoService.findOne(tipoProduto.getId());

		if(tipoProdutoFound == null){
			HttpHeaders headers = new HttpHeaders();
			headers.add(EXCEPTION_HEADER, "Categoria do produto não existe");
			return new ResponseEntity<Void>(headers, HttpStatus.NOT_FOUND);
		}

		produtoService.save(produto);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/produto/{id}").buildAndExpand(produto.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Alcanca um produto por id", response = Produto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/produto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> getById(@PathVariable Integer id) {
		Produto produto = produtoService.findOne(id);

		if (produto == null) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Produto>(produto, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Busca um produto por nome", response = Produto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/produto/nome/{nomeProduto}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Produto> getByName(@PathVariable String nomeProduto) {
		Produto produto = produtoService.findByNome(nomeProduto);

		if (produto == null) {
			return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Produto>(produto, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Atualiza os dados de um produto", response = Produto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/produto/update/{id}", method = RequestMethod.POST)
	public ResponseEntity<Produto> updateById(@PathVariable Integer id, @RequestBody Produto produto,
			UriComponentsBuilder ucBuilder) {
		Produto currentProduto = produtoService.updateById(id, produto);

		if (currentProduto != null) {
			HttpHeaders header = new HttpHeaders();
			header.setLocation(ucBuilder.path("/produto/{id}").buildAndExpand(produto.getId()).toUri());
			return new ResponseEntity<Produto>(header, HttpStatus.OK);
		}

		return new ResponseEntity<Produto>(HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value = "Remove um produto", response = Produto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Removido com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/produto/delete/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

		Produto produto = produtoService.findOne(id);
		if (produto == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		produtoService.delete(produto);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
	@ApiOperation(value = "Alcanca todos os produtos na base de dados", response = Produto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Listado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/produto/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Produto>> listAll() {
		List<Produto> produtoList = produtoService.listAll();

		if (produtoList.isEmpty()) {
			return new ResponseEntity<List<Produto>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Produto>>(produtoList, HttpStatus.OK);
	}
}
