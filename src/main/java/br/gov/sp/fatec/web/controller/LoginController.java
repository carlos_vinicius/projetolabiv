package br.gov.sp.fatec.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.security.JwtUtils;
import br.gov.sp.fatec.security.Login;
import br.gov.sp.fatec.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class LoginController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager auth;

	public void setAuth(AuthenticationManager auth) {
		this.auth = auth;
	}
	
	@ApiOperation(value = "Rota usada para autorizar o usuario", response = Usuario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Autorizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(path = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Usuario login(@RequestBody Login login, HttpServletResponse response) throws JsonProcessingException {
		Authentication credentials = new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());
		Usuario usuario = (Usuario) auth.authenticate(credentials).getPrincipal();
		usuario.setSenha(null);
		String token = JwtUtils.generateToken(usuario);
		response.setHeader("Token", token);
		return usuario;
	}
	
	@ApiOperation(value = "Rota usada para registrar um usuario", response = Usuario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Registrado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(path = "/registrar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> register(@RequestBody Login login, HttpServletResponse response){
		try {
			Usuario usuario = usuarioService.registrar(login.getUsername(), login.getPassword());
			return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		} catch (IllegalArgumentException e) {
			HttpHeaders headers = new HttpHeaders();
			headers.add("exceptionMessage", e.getMessage());
			return new ResponseEntity<Usuario>(headers, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@ApiOperation(value = "Rota usada para atualizar a senha do usuario", response = Usuario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/resgatarsenha", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Usuario> resetarsenha(@RequestBody ResgateSenhaWrapper resgateSenha) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Usuario usuario;
		try {
			usuario = usuarioService.resetarSenha(resgateSenha.getEmail(),
					formatter.parse(resgateSenha.getDataAdmissao()),
					resgateSenha.getNewPassword());
		} catch (IllegalArgumentException | ParseException e) {
			HttpHeaders header = new HttpHeaders();
			header.add("exceptionMessage", e.getMessage());
			return new ResponseEntity<Usuario>(header, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Rota usada para atrelar um funcionario ao usuario", response = Usuario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atrelado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/usuario/atrelarfuncionario", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> atrelarFuncionario(@RequestParam(value = "funcionarioEmail") String funcionarioEmail, Principal principal) {		
		if(principal != null && principal.getName() != null){
			Usuario usuario = usuarioService.findByNome(principal.getName());
			
			if(usuario == null){
				return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			}
			
			Usuario found = usuarioService.atrelarFuncionario(usuario.getId(), funcionarioEmail);
			if(found == null){
				return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
	}
}
