package br.gov.sp.fatec.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.Fornecedor;
import br.gov.sp.fatec.service.FornecedorService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class FornecedorController {

	@Autowired
	private FornecedorService fornecedorService;

	@ApiOperation(value = "Adiciona um Fornecedor", response = Fornecedor.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Adicionado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fornecedor/add", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> add(@RequestBody Fornecedor fornecedor, UriComponentsBuilder ucBuilder) {

		fornecedorService.save(fornecedor);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/fornecedor/{id}").buildAndExpand(fornecedor.getId()).toUri());
		return new ResponseEntity<Void>(header, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Lista todos Fornecedores", response = Fornecedor.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Listado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fornecedor/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Fornecedor>> findAll() {
		List<Fornecedor> fornecedorList = fornecedorService.findAll();
		
		if (fornecedorList.isEmpty()) {
			return new ResponseEntity<List<Fornecedor>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Fornecedor>>(fornecedorList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Atualiza os dados de um Fornecedor", response = Fornecedor.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fornecedor/update/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Fornecedor> updateById(@PathVariable Integer id, @RequestBody Fornecedor fornecedor,
			UriComponentsBuilder ucBuilder) {
		
		Fornecedor newFornecedor = fornecedorService.updateById(id, fornecedor);
		HttpHeaders header = new HttpHeaders();

		if (newFornecedor != null){
			header.setLocation(ucBuilder.path("/fornecedor/{id}").buildAndExpand(fornecedor.getId()).toUri());
			return new ResponseEntity<Fornecedor>(header, HttpStatus.OK);
		}
		else{
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Alcanca um Fornecedor por Id", response = Fornecedor.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fornecedor/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Fornecedor> findById(@PathVariable Integer id) {
		
		Fornecedor fornecedor = fornecedorService.findOne(id);

		if (fornecedor == null) {
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Fornecedor>(fornecedor, HttpStatus.FOUND);
	}
	
	@ApiOperation(value = "Remove um Fornecedor", response = Fornecedor.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Removido com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/fornecedor/delete/{id}", method = RequestMethod.PUT)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

		Fornecedor fornecedor = fornecedorService.findOne(id);
		if (fornecedor == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		fornecedorService.delete(fornecedor);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	
}
