package br.gov.sp.fatec.web.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.Funcionario;
import br.gov.sp.fatec.service.FuncionarioService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class FuncionarioController {

	@Autowired
	private FuncionarioService funcionarioService;
	
	@ApiOperation(value = "Adiciona um funcionario", response = Funcionario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Adicionado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/funcionario/add", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> add(@RequestBody Funcionario funcionario, UriComponentsBuilder ucBuilder) {
		funcionario.setDataAdmissao(new Date());

		funcionarioService.save(funcionario);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/funcionario/{id}").buildAndExpand(funcionario.getId()).toUri());
		return new ResponseEntity<Void>(header, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Atualiza os dados de um funcionario", response = Funcionario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/funcionario/update/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Funcionario> updateById(@PathVariable Integer id, @RequestBody Funcionario funcionario,
			UriComponentsBuilder ucBuilder) {
		Funcionario currentFuncionario = funcionarioService.updateById(id.longValue(), funcionario);

		if (currentFuncionario != null) {
			HttpHeaders header = new HttpHeaders();
			header.setLocation(
					ucBuilder.path("/funcionario/{id}").buildAndExpand(currentFuncionario.getId()).toUri());
			return new ResponseEntity<Funcionario>(header, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Alcanca um funcionario por id", response = Funcionario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/funcionario/{id}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Funcionario> findById(@PathVariable Integer id) {
		Funcionario funcionario = funcionarioService.findOne(id.longValue());

		if (funcionario == null) {
			return new ResponseEntity<Funcionario>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Funcionario>(funcionario, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Lista todos os funcionarios", response = Funcionario.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Listado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/funcionario/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Funcionario>> listAll() {
		List<Funcionario> funcionarioList = funcionarioService.listAll();

		if (funcionarioList.isEmpty()) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Funcionario>>(funcionarioList, HttpStatus.OK);
	}	
}
