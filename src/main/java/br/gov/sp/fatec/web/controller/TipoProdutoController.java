package br.gov.sp.fatec.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.gov.sp.fatec.model.TipoProduto;
import br.gov.sp.fatec.service.TipoProdutoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class TipoProdutoController {

	@Autowired
	private TipoProdutoService tipoProdutoService;
	
	@ApiOperation(value = "Adiciona um TipoProduto", response = TipoProduto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Adicionado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tipoprodutoproduto/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<TipoProduto> add(@RequestBody TipoProduto tipoProduto,
			UriComponentsBuilder ucBuilder) {
		tipoProdutoService.save(tipoProduto);

		HttpHeaders header = new HttpHeaders();
		header.setLocation(ucBuilder.path("/categoriaproduto/{id}").buildAndExpand(tipoProduto.getId()).toUri());
		return new ResponseEntity<TipoProduto>(header, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Lista todos TipoProduto", response = TipoProduto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Lista alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tipoprodutoproduto/todos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<TipoProduto>> findAll() {
		List<TipoProduto> tipoProdutoList = tipoProdutoService.findAll();
		
		if (tipoProdutoList.isEmpty()) {
			return new ResponseEntity<List<TipoProduto>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<TipoProduto>>(tipoProdutoList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Atualiza os dados de um TipoProduto", response = TipoProduto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Atualizado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tipoprodutoproduto/update/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<TipoProduto> updateById(@PathVariable Integer id, @RequestBody TipoProduto tipoProduto,
			UriComponentsBuilder ucBuilder) {
		
		TipoProduto newTipoProduto = tipoProdutoService.updateById(id, tipoProduto);
		HttpHeaders header = new HttpHeaders();

		if (newTipoProduto != null){
			header.setLocation(ucBuilder.path("/tipoprodutoproduto/{id}").buildAndExpand(tipoProduto.getId()).toUri());
			return new ResponseEntity<TipoProduto>(header, HttpStatus.OK);
		}
		else{
			return new ResponseEntity<TipoProduto>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Alcanca um TipoProduto por id", response = TipoProduto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancado com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tipoprodutoproduto/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<TipoProduto> findById(@PathVariable Integer id) {
		
		TipoProduto tipoProduto = tipoProdutoService.findOne(id);

		if (tipoProduto == null) {
			return new ResponseEntity<TipoProduto>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<TipoProduto>(tipoProduto, HttpStatus.FOUND);
	}
	
	@ApiOperation(value = "Remove um TipoProduto por id", response = TipoProduto.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Removido com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/tipoprodutoproduto/delete/{id}", method = RequestMethod.PUT)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

		TipoProduto tipoProduto = tipoProdutoService.findOne(id);
		if (tipoProduto == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		tipoProdutoService.delete(tipoProduto);
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
}