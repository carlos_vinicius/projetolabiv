package br.gov.sp.fatec.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.gov.sp.fatec.model.Movimento;
import br.gov.sp.fatec.service.MovimentoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class MovimentoController {

	@Autowired
	private MovimentoService movimentoService;
	
	
	@ApiOperation(value = "Alcanca um movimento de caixa por id", response = Movimento.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/movimento/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<Movimento> getCaixaById(@PathVariable Integer id) {

		Movimento movimento = movimentoService.findOne(id);

		if (movimento == null) {
			return new ResponseEntity<Movimento>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Movimento>(movimento, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Alcanca um relatorio de entradas positivas no caixa", response = Movimento.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/movimento/entrada", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Movimento>> gerarRelatorioEntrada(@RequestParam Integer dias) {

		List<Movimento> movimentosEntrada = movimentoService.relatorioEntrada(dias);

		if (movimentosEntrada.isEmpty()) {
			return new ResponseEntity<List<Movimento>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Movimento>>(movimentosEntrada, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Alcanca um relatorio de entradas negativas no caixa", response = Movimento.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/movimento/saida", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Movimento>> gerarRelatorioSaida(@RequestParam Integer dias) {

		List<Movimento> movimentosEntrada = movimentoService.relatorioSaida(dias);

		if (movimentosEntrada.isEmpty()) {
			return new ResponseEntity<List<Movimento>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Movimento>>(movimentosEntrada, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Alcanca um relatorio de todas as entradas no caixa", response = Movimento.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Alcancada com sucesso"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/movimento/geral", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("isAuthenticated()")
	public ResponseEntity<List<Movimento>> gerarRelatorioGeral(@RequestParam Integer dias) {

		List<Movimento> movimentosEntrada = movimentoService.relatorioGeral(dias);

		if (movimentosEntrada.isEmpty()) {
			return new ResponseEntity<List<Movimento>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Movimento>>(movimentosEntrada, HttpStatus.OK);
	}
	
}
