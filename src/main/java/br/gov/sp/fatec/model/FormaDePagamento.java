package br.gov.sp.fatec.model;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum FormaDePagamento {
	DINHEIRO, CHEQUE, CARTAO_DEBITO, CARTAO_CREDITO
}
