package br.gov.sp.fatec.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;


@Entity
@Table(name = "tipoProduto")
@ApiModel
public class TipoProduto implements Serializable {

	private static final long serialVersionUID = 6826668362654798912L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idTipoProduto", nullable = false, unique = true)
	private Integer id;

	@Column(name = "nome", length = 50)
	private String nome;
	
	@Column(name = "tipo", length = 150)
	private String tipo;

	@Column(name = "descricao", length = 250)
	private String descricao;
	
	@OneToMany(mappedBy = "tipoProduto", fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Produto> listaDeProdutos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Produto> getListaDeProdutos() {
		return listaDeProdutos;
	}

	public void setListaDeProdutos(List<Produto> listaDeProdutos) {
		this.listaDeProdutos = listaDeProdutos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}