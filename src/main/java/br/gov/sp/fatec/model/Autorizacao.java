package br.gov.sp.fatec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import io.swagger.annotations.ApiModel;

@Entity
@Table(name = "autorizacao")
@ApiModel
public class Autorizacao implements GrantedAuthority {

	private static final long serialVersionUID = -7578937961979778761L;

	@Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idAutorizacao")
	private Long id;
    
    @Column(name = "nome", unique=true, length = 20, nullable = false)
    private String nome;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getAuthority() {
		return this.nome;
	}
	
	public void setAuthority(String authority) {
		this.nome = authority;
	}

}
