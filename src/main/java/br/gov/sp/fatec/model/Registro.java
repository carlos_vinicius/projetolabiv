package br.gov.sp.fatec.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import io.swagger.annotations.ApiModel;

@Entity
@Table(name = "registro")
@ApiModel
public class Registro implements Serializable {

	private static final long serialVersionUID = 6020475795750011672L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRegistro", nullable = false, unique = true)
	private Integer idRegistro;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
	private Cliente cliente;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idFuncionario", referencedColumnName = "idFuncionario")
	private Funcionario funcionario;

	@ManyToOne(fetch = FetchType.EAGER) //Eager pois eh soh um, lazy causa uma exception
	@JoinColumn(name = "idProduto", referencedColumnName = "idProduto")
	private Produto produto;

	@Enumerated(EnumType.STRING)
	@Column(name = "formaDePagamento", nullable = false)
	private FormaDePagamento formaDePagamento;

	@Temporal(TemporalType.DATE)
	@Column(name = "dataRegistro")
	private Date data;
	
	@Column(name = "quantidade", nullable = false)
	private Integer quantidade;

	public Integer getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Integer id) {
		this.idRegistro = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario idfuncionario) {
		this.funcionario = idfuncionario;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public FormaDePagamento getFormaDePagamento() {
		return formaDePagamento;
	}

	public void setFormaDePagamento(FormaDePagamento formaDePagamento) {
		this.formaDePagamento = formaDePagamento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
}
