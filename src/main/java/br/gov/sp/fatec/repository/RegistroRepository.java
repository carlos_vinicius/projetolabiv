package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Registro;

@Repository
public interface RegistroRepository extends JpaRepository<Registro, Integer> {

	public List<Registro> findAll();

}
