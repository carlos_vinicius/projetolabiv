package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

	public Funcionario findByNome(String username);

	public List<Funcionario> findAllByOrderByNomeAsc();

	public Funcionario findByEmail(String email);
	
}
