package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Estoque;
import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;

@Repository
public interface EstoqueRepository extends JpaRepository<Estoque, Integer> {
	
	public Estoque findByProduto(Produto produto);
	public Estoque findByProdutoId(Integer produtoId);
	public List<Estoque> findByProdutoTipoProduto(TipoProduto tipoProduto);
}
