package br.gov.sp.fatec.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
	public Produto findByNome(String nome);
	public List<Produto> findByOrderByNomeAsc();
	public List<Produto> findByTipoProduto(TipoProduto tipoProduto);
}
