package br.gov.sp.fatec.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Movimento;

@Repository
public interface MovimentoRepository extends JpaRepository<Movimento, Integer> {

	public List<Movimento> findByDataMovimentoAfter(@Temporal(TemporalType.DATE) Date dataMaxima);
	
	@Query("Select m from Movimento m where m.saldo > 0 and m.dataMovimento > ?1")
	public List<Movimento> findPositivo(@Temporal(TemporalType.DATE) Date dataMaxima);
	
	@Query("Select m from Movimento m where m.saldo < 0 and m.dataMovimento > ?1")
	public List<Movimento> findNegativo(@Temporal(TemporalType.DATE) Date dataMaxima);

}
