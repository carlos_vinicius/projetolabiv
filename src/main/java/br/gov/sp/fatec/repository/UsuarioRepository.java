package br.gov.sp.fatec.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	public Usuario findByNome(String nome);
	
	@Query("select u from Usuario u join u.funcionario a where (a.email = ?1 AND a.dataAdmissao = ?2)")
	public Usuario encontrarByEmailAndAdmissao(String email, Date dataAdmissao);

}
