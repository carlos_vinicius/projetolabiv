package br.gov.sp.fatec.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.sp.fatec.model.Fornecedor;


@Repository
public interface FornecedorRepository extends JpaRepository<Fornecedor, Integer> {

}
