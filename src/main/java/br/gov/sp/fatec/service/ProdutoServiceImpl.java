package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;
import br.gov.sp.fatec.repository.ProdutoRepository;

@Service
@Transactional
public class ProdutoServiceImpl implements ProdutoService{

	@Autowired
	private ProdutoRepository produtoRepository;

	@Override
	public Produto save(Produto produto) {
		return produtoRepository.save(produto);
	}

	@Override
	public Produto findOne(int id) {
		return produtoRepository.findOne(id);
	}

	@Override
	public Produto findByNome(String nome) {
		return produtoRepository.findByNome(nome);
	}

	@Override
	public void delete(Produto produto) {
		produtoRepository.delete(produto);

	}

	@Override
	public Produto updateById(Integer id, Produto produto) {
		Produto newProduto = produtoRepository.findOne(id);
		
		if (newProduto != null) {
			newProduto.setNome(produto.getNome());
			newProduto.setPreco(produto.getPreco());
			//newProduto.setTipoProduto(produto.getTipoProduto());
			newProduto.setFabricante(produto.getFabricante());
			
			produtoRepository.save(newProduto);
			return newProduto;
		}
		else{
			return null;
		}
	}

	@Override
	public List<Produto> listAll() {
		return produtoRepository.findByOrderByNomeAsc();
	}

	@Override
	public List<Produto> listAllbyTipo(TipoProduto tipoProduto) {
		return produtoRepository.findByTipoProduto(tipoProduto);
	}

}
