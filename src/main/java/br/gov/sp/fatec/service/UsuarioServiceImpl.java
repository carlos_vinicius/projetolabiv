package br.gov.sp.fatec.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import br.gov.sp.fatec.model.Autorizacao;
import br.gov.sp.fatec.model.Funcionario;
import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.repository.AutorizacaoRepository;
import br.gov.sp.fatec.repository.FuncionarioRepository;
import br.gov.sp.fatec.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	private static final String ROLE_USUARIO = "ROLE_USUARIO";
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private AutorizacaoRepository autorizacaoRepository;
	
	@Autowired
	private FuncionarioRepository funcionarioRepository;

	@Override
	public Usuario registrar(String login, String password) throws IllegalArgumentException{
		if(usuarioRepository.findByNome(login) != null){
			throw new IllegalArgumentException("O Usuário já existe");
		};
		
		Md5PasswordEncoder pe = new Md5PasswordEncoder();
		
		Autorizacao autorizacao = autorizacaoRepository.findFirstByNome(ROLE_USUARIO);
		ArrayList<Autorizacao> list = new ArrayList<Autorizacao>();
		list.add(autorizacao);
		
		Usuario usuario = new Usuario();
		usuario.setNome(login);
		usuario.setSenha(pe.encodePassword(password, null));
		usuario.setAutorizacoes(list);
		usuarioRepository.save(usuario);
		return usuario;
	}
	
	

	@Override
	public Usuario resetarSenha(String email, Date dataAdmissao, String novoPassword) throws IllegalArgumentException{
		Usuario usuario = usuarioRepository.encontrarByEmailAndAdmissao(email, dataAdmissao);
		
		if(usuario == null){
			throw new IllegalArgumentException("O Usuário nao existe ou os dados nao conferem");
		};
		
		Md5PasswordEncoder pe = new Md5PasswordEncoder();
		
		usuario.setSenha(pe.encodePassword(novoPassword, null));
		
		usuarioRepository.save(usuario);
		return usuario;
	}

	@Override
	public Usuario save(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	@Override
	public Usuario atrelarFuncionario(Long usuarioId, String funcionarioEmail) {
		Funcionario funcionarioFound = funcionarioRepository.findByEmail(funcionarioEmail);
		Usuario usuarioFound = usuarioRepository.findOne(usuarioId);
		if(funcionarioFound != null && usuarioFound != null){
			usuarioFound.setFuncionario(funcionarioFound);
			usuarioRepository.save(usuarioFound);
			return usuarioFound;
		}
		
		return null;	
	}
	
	@Override
	public Usuario atrelarFuncionarioById(Long usuarioId, Long funcionarioId) {
		Funcionario funcionarioFound = funcionarioRepository.findOne(funcionarioId);
		Usuario usuarioFound = usuarioRepository.findOne(usuarioId);
		if(funcionarioFound != null && usuarioFound != null){
			usuarioFound.setFuncionario(funcionarioFound);
			usuarioRepository.save(usuarioFound);
			return usuarioFound;
		}
		
		return null;	
	}

	@Override
	public Usuario findByNome(String nome) {
		return usuarioRepository.findByNome(nome);
	}
	
}
