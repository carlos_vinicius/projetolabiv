package br.gov.sp.fatec.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Estoque;
import br.gov.sp.fatec.model.Fornecedor;
import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;
import br.gov.sp.fatec.model.Movimento;
import br.gov.sp.fatec.repository.EstoqueRepository;
import javassist.NotFoundException;

@Service
@Transactional
public class EstoqueServiceImpl implements EstoqueService {

	@Autowired
	private EstoqueRepository estoqueRepository;
	
	@Autowired
	private MovimentoService movimentoService;
	
	@Override
	public Estoque save(Estoque estoque){
		return estoqueRepository.save(estoque);
	}
	
	@Override
	public Estoque criar(Produto produto, Fornecedor fornecedor, Integer quantidade){
		Estoque estoque = new Estoque();
		estoque.setFornecedor(fornecedor);
		estoque.setProduto(produto);
		estoque.setQuantidade(quantidade);
		
		estoqueRepository.save(estoque);
		
		Double totalMovimento = - (produto.getPreco() * quantidade);
		Movimento movimento = new Movimento();
		movimento.setSaldo(totalMovimento);
		movimento.setDataMovimento(new Date());
		movimentoService.save(movimento);

		return estoque;
	}

	@Override
	public boolean possuiEstoque(Produto produto, Integer quantidade) throws NotFoundException {
		Estoque estoque = estoqueRepository.findByProduto(produto);
		if(estoque == null)
			throw new NotFoundException("Nao ha instancia do estoque no banco de dados");
		if(estoque.getQuantidade() > quantidade)
			return true;
		return false;
	}

	@Override
	public void debitarEstoque(Produto produto, Integer quantidade) throws NotFoundException {
		Estoque estoque = estoqueRepository.findByProduto(produto);
		if(estoque == null)
			throw new NotFoundException("Nao ha instancia do estoque no banco de dados");

		if(estoque.getQuantidade() > quantidade && estoque.getProduto().getId() == produto.getId()){
			estoque.setQuantidade(estoque.getQuantidade() - quantidade);
			estoqueRepository.save(estoque);
			Double totalMovimento = produto.getPreco() * quantidade;
			Movimento movimento = new Movimento();
			movimento.setSaldo(totalMovimento);
			movimento.setDataMovimento(new Date());
			movimentoService.save(movimento);
		}
	}

	@Override
	public Estoque findOne(Integer id) {
		return estoqueRepository.findOne(id);
	}

	@Override
	public void reporEstoques(Integer quantidade) {
		for (Iterator<Estoque> iterator = estoqueRepository.findAll().iterator(); iterator.hasNext();) {
			Estoque estoque = iterator.next();
			if(estoque.getQuantidade() < quantidade){
				estoque.setQuantidade(quantidade);
				estoqueRepository.save(estoque);
				Double totalMovimento = - (estoque.getProduto().getPreco() * quantidade);
				Movimento movimento = new Movimento();
				movimento.setSaldo(totalMovimento);
				movimento.setDataMovimento(new Date());
				movimentoService.save(movimento);
			}
		}
	}

	@Override
	public void reporEstoqueProduto(Integer produtoId, Integer quantidade) throws NotFoundException {
		Estoque estoque = estoqueRepository.findByProdutoId(produtoId);
		if(estoque == null)
			throw new NotFoundException("Nao ha instancia do estoque no banco de dados");
		if(estoque.getQuantidade() < quantidade){
			estoque.setQuantidade(quantidade);
			estoqueRepository.save(estoque);
			Double totalMovimento = - (estoque.getProduto().getPreco() * quantidade);
			Movimento movimento = new Movimento();
			movimento.setSaldo(totalMovimento);
			movimento.setDataMovimento(new Date());
			movimentoService.save(movimento);
		}
	}

	@Override
	public List<Estoque> gerarRelatorio() {
		return estoqueRepository.findAll();
	}

	@Override
	public List<Estoque> gerarRelatorioPorTipo(TipoProduto tipoProduto) {
		return estoqueRepository.findByProdutoTipoProduto(tipoProduto);
	}
}
