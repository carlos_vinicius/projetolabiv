package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Movimento;

public interface MovimentoService {

	public Movimento findOne(Integer id);
	public Movimento save(Movimento movimento);
	public List<Movimento> relatorioEntrada(Integer dias);
	public List<Movimento> relatorioSaida(Integer dias);
	public List<Movimento> relatorioGeral(Integer dias);

}
