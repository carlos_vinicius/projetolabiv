package br.gov.sp.fatec.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Movimento;
import br.gov.sp.fatec.repository.MovimentoRepository;

@Service
@Transactional
public class MovimentoServiceImpl implements MovimentoService{
	
	@Autowired
	private MovimentoRepository movimentoRepository;

	@Override
	public Movimento findOne(Integer id) {
		return movimentoRepository.findOne(id);
	}

	@Override
	public Movimento save(Movimento movimento) {
		return movimentoRepository.save(movimento);
	}

	@Override
	public List<Movimento> relatorioEntrada(Integer dias) {
		long diaEmMili = 1000 * 60 * 60 * 24;
		Date dataMaxima = new Date(System.currentTimeMillis() - dias * diaEmMili);
		return movimentoRepository.findPositivo(dataMaxima);
	}

	@Override
	public List<Movimento> relatorioSaida(Integer dias) {
		long diaEmMili = 1000 * 60 * 60 * 24;
		Date dataMaxima = new Date(System.currentTimeMillis() - dias * diaEmMili);
		return movimentoRepository.findNegativo(dataMaxima);
	}

	@Override
	public List<Movimento> relatorioGeral(Integer dias) {
		long diaEmMili = 1000 * 60 * 60 * 24;
		Date dataMaxima = new Date(System.currentTimeMillis() - dias * diaEmMili);
		return movimentoRepository.findByDataMovimentoAfter(dataMaxima);
	}	
}
