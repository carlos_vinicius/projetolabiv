package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.TipoProduto;
import br.gov.sp.fatec.repository.TipoProdutoRepository;

@Service
@Transactional
public class TipoProdutoServiceImpl implements TipoProdutoService{
	
	@Autowired
	private TipoProdutoRepository tipoProdutoRepository;

	@Override
	public TipoProduto save(TipoProduto tipoProduto) {
		return tipoProdutoRepository.save(tipoProduto);
	}

	@Override
	public List<TipoProduto> findAll() {
		return tipoProdutoRepository.findAll();
	}

	@Override
	public TipoProduto updateById(Integer id, TipoProduto tipoProduto) {

		TipoProduto currentTipoProduto = tipoProdutoRepository.findOne(id);

		if (currentTipoProduto != null) {
			currentTipoProduto.setNome(tipoProduto.getNome());
			currentTipoProduto.setDescricao(tipoProduto.getDescricao());
			currentTipoProduto.setTipo(tipoProduto.getTipo());
			
			tipoProdutoRepository.save(currentTipoProduto);
			return currentTipoProduto;
		}
		else{
			return null;
		}
	}

	@Override
	public TipoProduto findOne(Integer id) {
		return tipoProdutoRepository.findOne(id);
	}

	@Override
	public void delete(TipoProduto tipoProduto) {
		tipoProdutoRepository.delete(tipoProduto);
	}

}
