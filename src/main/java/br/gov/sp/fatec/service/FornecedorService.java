package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Fornecedor;

public interface FornecedorService {

	public Fornecedor save(Fornecedor fornecedor);

	public List<Fornecedor> findAll();

	public Fornecedor updateById(Integer id, Fornecedor fornecedor);

	public Fornecedor findOne(Integer id);

	public void delete(Fornecedor fornecedor);

}
