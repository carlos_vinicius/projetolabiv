package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.TipoProduto;


public interface TipoProdutoService {

	public TipoProduto save(TipoProduto tipoProduto);

	public List<TipoProduto> findAll();

	public TipoProduto updateById(Integer id, TipoProduto tipoProduto);

	public TipoProduto findOne(Integer id);

	public void delete(TipoProduto tipoProduto);
	
}
