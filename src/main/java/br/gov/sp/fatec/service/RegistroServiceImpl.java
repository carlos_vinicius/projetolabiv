package br.gov.sp.fatec.service;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Registro;
import br.gov.sp.fatec.repository.RegistroRepository;
import javassist.NotFoundException;

@Service
@Transactional
public class RegistroServiceImpl implements RegistroService {
	
	@Autowired
	private RegistroRepository registroRepository;
	
	@Autowired
	private EstoqueService estoqueService;
	
	@Override
	public List<Registro> findAll() {
		return (List<Registro>) registroRepository.findAll();
	}

	@Override
	public Registro findOne(int id) {
		return registroRepository.findOne(id);
	}

	@Override
	public Registro save(Registro registro) {
		return registroRepository.save(registro);
	}

	@Override
	public void delete(Registro registro) {
		registroRepository.delete(registro);
	}
	
	@Override
	public Registro efetuaVenda(Registro registro) throws NoSuchElementException, IllegalArgumentException, NotFoundException{
		if(registro.getCliente() == null
				||registro.getProduto() == null
				||registro.getFuncionario() == null
				||registro.getFormaDePagamento() == null
				||registro.getQuantidade() < 1){
			throw new IllegalArgumentException("Registro nao possui dados suficientes");
		}
		
		if(registro.getProduto().getId() == null 
				|| registro.getCliente().getId() == null 
				|| registro.getFuncionario().getId() == null ){
			throw new NoSuchElementException("Os argumentos do registro nao possuem id ou nao tem instancia no banco de dados");
		}
		
		estoqueService.possuiEstoque(registro.getProduto(), registro.getQuantidade());
		registro.setData(new Date());
		registroRepository.save(registro);
		estoqueService.debitarEstoque(registro.getProduto(), registro.getQuantidade());
		return registro;
	}

	@Override
	public List<Registro> listAll() {
		return registroRepository.findAll();
	}

}
