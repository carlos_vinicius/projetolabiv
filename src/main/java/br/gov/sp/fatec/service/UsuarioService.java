package br.gov.sp.fatec.service;

import java.util.Date;

import br.gov.sp.fatec.model.Usuario;

public interface UsuarioService {
	
	public Usuario registrar(String login, String password);
	public Usuario resetarSenha(String email, Date dataAdmissao, String newPassword) throws IllegalArgumentException;
	public Usuario save(Usuario usuario);
	public Usuario atrelarFuncionario(Long usuarioId, String funcionarioEmail);
	public Usuario atrelarFuncionarioById(Long usuarioId, Long funcionarioId);
	public Usuario findByNome(String nome);
}
