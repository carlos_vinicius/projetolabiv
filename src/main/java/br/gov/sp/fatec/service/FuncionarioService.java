package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Funcionario;

public interface FuncionarioService{
	public Funcionario save(Funcionario funcionario);
	public Funcionario findOne(Long id);
	public void delete(Funcionario funcionario);
	public Funcionario updateById(Long id, Funcionario funcionario);
	public List<Funcionario> listAll();
	public Funcionario findByEmail(String email);
}
