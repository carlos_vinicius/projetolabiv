package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Estoque;
import br.gov.sp.fatec.model.Fornecedor;
import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;
import javassist.NotFoundException;

public interface EstoqueService {

	public Estoque save(Estoque estoque);
	public Estoque findOne(Integer id);
	public void debitarEstoque(Produto produto, Integer quantidade) throws NotFoundException;
	public boolean possuiEstoque(Produto produto, Integer quantidade) throws NotFoundException;
	public void reporEstoques(Integer quantidade);
	public void reporEstoqueProduto(Integer produtoId, Integer quantidade) throws NotFoundException;
	public Estoque criar(Produto produto, Fornecedor fornecedor, Integer quantidade);
	public List<Estoque> gerarRelatorio();
	public List<Estoque> gerarRelatorioPorTipo(TipoProduto tipoProduto);
}
