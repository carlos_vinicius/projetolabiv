package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Funcionario;
import br.gov.sp.fatec.repository.FuncionarioRepository;

@Service
@Transactional
public class FuncionarioServiceImpl implements FuncionarioService{

	@Autowired
	private FuncionarioRepository funcionarioRepository;
	
	@Override
	public Funcionario save(Funcionario funcionario) {
		return funcionarioRepository.save(funcionario);
	}

	@Override
	public Funcionario findOne(Long id) {
		return funcionarioRepository.findOne(id);
	}

	@Override
	public void delete(Funcionario funcionario) {
		funcionarioRepository.delete(funcionario);
	}
	
	@Override
	public Funcionario findByEmail(String email) {
		return funcionarioRepository.findByEmail(email);
	}

	@Override
	public Funcionario updateById(Long id, Funcionario funcionario) {

		Funcionario newFuncionario = funcionarioRepository.findOne(id);

		if (newFuncionario != null) {
			newFuncionario.setNome(funcionario.getNome());
			newFuncionario.setSobrenome(funcionario.getSobrenome());
			newFuncionario.setEmail(funcionario.getEmail());
			newFuncionario.setEndereco(funcionario.getEndereco());
			newFuncionario.setSalario(funcionario.getSalario());
			
			funcionarioRepository.save(newFuncionario);
			return newFuncionario;
		}
		else{
			return null;
		}
	}

	@Override
	public List<Funcionario> listAll() {
		return funcionarioRepository.findAllByOrderByNomeAsc();
	}
}
