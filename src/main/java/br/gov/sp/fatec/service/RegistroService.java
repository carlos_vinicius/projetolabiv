package br.gov.sp.fatec.service;

import java.util.List;
import java.util.NoSuchElementException;

import br.gov.sp.fatec.model.Registro;
import javassist.NotFoundException;

public interface RegistroService{
	public List<Registro> findAll();
	public Registro findOne(int id);
	public Registro save(Registro registro);
	public void delete(Registro registro);
	public Registro efetuaVenda(Registro registro) throws NoSuchElementException, IllegalArgumentException, NotFoundException;
	public List<Registro> listAll();
}
