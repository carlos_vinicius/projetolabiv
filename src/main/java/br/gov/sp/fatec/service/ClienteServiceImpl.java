package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Cliente;
import br.gov.sp.fatec.repository.ClienteRepository;


@Service
@Transactional
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public Cliente findOne(int id) {
		return clienteRepository.findOne(id);
	}

	@Override
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public void delete(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
	
	@Override
	public Cliente updateById(Integer id, Cliente cliente) {
		
		Cliente currentCliente = clienteRepository.findOne(id);

		if (currentCliente != null) {
			currentCliente.setNome(cliente.getNome());
			currentCliente.setSobrenome(cliente.getSobrenome());
			currentCliente.setEmail(cliente.getEmail());
			currentCliente.setEndereco(cliente.getEndereco());
			
			clienteRepository.save(currentCliente);
			return currentCliente;
		}
		else{
			return null;
		}
	}

	@Override
	public List<Cliente> listAll() {
		return clienteRepository.findAllByOrderByNomeAsc();
	}
}
