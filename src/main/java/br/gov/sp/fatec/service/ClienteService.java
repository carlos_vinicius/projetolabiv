package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Cliente;

public interface ClienteService{
	public Cliente findOne(int id);
	public Cliente save(Cliente cliente);
	public void delete(Cliente cliente);
	public Cliente updateById(Integer id, Cliente cliente);
	public List<Cliente> listAll();
}
