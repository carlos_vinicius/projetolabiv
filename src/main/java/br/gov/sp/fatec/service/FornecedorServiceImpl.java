package br.gov.sp.fatec.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Fornecedor;
import br.gov.sp.fatec.repository.FornecedorRepository;

@Service
@Transactional
public class FornecedorServiceImpl implements FornecedorService{
	
	@Autowired
	private FornecedorRepository fornecedorRepository;
	
	@Override
	public Fornecedor save(Fornecedor fornecedor){
		return fornecedorRepository.save(fornecedor);
	}

	@Override
	public List<Fornecedor> findAll() {
		return fornecedorRepository.findAll();
	}

	@Override
	public Fornecedor updateById(Integer id, Fornecedor fornecedor) {

		Fornecedor currentFornecedor = fornecedorRepository.findOne(id);

		if (currentFornecedor != null) {
			currentFornecedor.setNome(fornecedor.getNome());
			currentFornecedor.setEmail(fornecedor.getEmail());
			currentFornecedor.setEndereco(fornecedor.getEndereco());
			
			fornecedorRepository.save(currentFornecedor);
			return currentFornecedor;
		}
		else{
			return null;
		}
	}

	@Override
	public Fornecedor findOne(Integer id) {
		return fornecedorRepository.findOne(id);
	}

	@Override
	public void delete(Fornecedor fornecedor) {
		fornecedorRepository.delete(fornecedor);
	}
}
