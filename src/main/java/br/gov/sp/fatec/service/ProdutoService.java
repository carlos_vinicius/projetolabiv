package br.gov.sp.fatec.service;

import java.util.List;

import br.gov.sp.fatec.model.Produto;
import br.gov.sp.fatec.model.TipoProduto;

public interface ProdutoService{
	public Produto save(Produto produto);
	public Produto findOne(int id);
	public Produto findByNome(String nome);
	public void delete(Produto produto);
	public Produto updateById(Integer id, Produto produto);
	public List<Produto> listAll();
	public List<Produto> listAllbyTipo(TipoProduto tipoProduto);
}
