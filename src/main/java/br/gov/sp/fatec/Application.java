package br.gov.sp.fatec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import br.gov.sp.fatec.security.RestAuthenticationEntryPoint;
import br.gov.sp.fatec.security.SimpleCORSFilter;

@SpringBootApplication
@ComponentScan("br.gov.sp.fatec")
@ImportResource({"classpath:applicationContext.xml", "classpath:applicationContext-security.xml"})
public class Application {
	
	@Bean
	public RestAuthenticationEntryPoint restAuthenticationEntryPoint() {
	    return new RestAuthenticationEntryPoint();              
	}
	
	@Bean
	public SimpleCORSFilter simpleCORSFilter() {
	    return new SimpleCORSFilter();              
	}

	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
