# Projeto de Lab IV - 1º Semestre 2017 #

### Qual objetivo do repositório? ###

* Esse repositório tem por objetivo armazenar todo o projeto para a aula de projeto de labotório IV.

### Como rodar? ###

Entre no diretório do projeto que você clonou neste repositório e execute o seguinte comando:
 
* mvn clean install
* mvn liquibase:update
* mvn spring-boot:run

### Contribuidores ###

* Carlos Vinícius
* Raphael Lucas
* Victor Jordan